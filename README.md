## Python, CNN, MNIST
This project was written using PyTorch.

The task was to prepare a program and conduct a series of experiments to classify handwritten numbers from the MNIST collection.

The other goal was comparison of the effectiveness and results of own and existing convolutional neural networks architecture (ResNet18).