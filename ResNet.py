# -*- coding: utf-8 -*-

import torch
import torchvision.models.resnet
from torch import nn
from torch import optim
from torchvision import transforms
from torchsummary import summary

import Service


def initialize_model(number_of_output_classes, feature_extracting, use_pretrained=True):
    model_to_return = torchvision.models.resnet18(pretrained=use_pretrained)
    if feature_extracting:
        for parameter in model_to_return.parameters():
            parameter.requires_grad = False
    num_features = model_to_return.fc.in_features
    model_to_return.fc = nn.Linear(num_features, number_of_output_classes)
    return model_to_return


def prepare_parameters_in_model(input_model):
    parameters = input_model.parameters()
    print("Parameters to learn:")
    if feature_extract:
        parameters = []
        for name, parameter in input_model.named_parameters():
            if parameter.requires_grad:
                parameters.append(parameter)
                print(name)
    else:
        for name, parameter in input_model.named_parameters():
            if parameter.requires_grad:
                print(name)
    print()
    return parameters


epochs = 25
learning_rate = 0.001
momentum = 0.9
batch_size = 50
load_model = False
feature_extract = True  # If True teach only the last layer of the network
model_path = './rn_' + str(epochs) + '_' + str(learning_rate) + '_' + str(batch_size) \
             + '_' + str(momentum) + '_' + str(feature_extract) + '.pt'
transform = transforms.Compose([transforms.Resize(224), transforms.ToTensor(),
                                transforms.Lambda(lambda x: x.repeat(3, 1, 1)),
                                transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
data_loaders = Service.load_data(transform, batch_size)

model_to_train = initialize_model(10, feature_extract, use_pretrained=True)
parameters_to_update = prepare_parameters_in_model(model_to_train)
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(parameters_to_update, lr=learning_rate, momentum=momentum)

if not load_model:
    model = Service.train_model(model_to_train, data_loaders['train'], criterion, optimizer, epochs, model_path)
    Service.test_model(model, data_loaders['val'], batch_size, False)
else:
    try:
        model = torch.load(model_path)
    except FileNotFoundError:
        model = Service.train_model(model_to_train, data_loaders['train'], criterion, optimizer, epochs, model_path)
    Service.test_model(model, data_loaders['val'], batch_size, False)
    # summary(model, (3, 224, 224))
