import copy
from time import time

import matplotlib.pyplot as plt
import numpy as np
import pandas
import seaborn
import torch
from sklearn.metrics import confusion_matrix
from torchvision import datasets


def plot_confusion_matrix(cm, data_type):
    confusion_matrix_data_frame = pandas.DataFrame(cm)
    if data_type == 'train':
        plt.title('Macierz pomyłek zestawu treningowego')
    else:
        plt.title('Macierz pomyłek zestawu testowego')
    seaborn.heatmap(confusion_matrix_data_frame, annot=True, cmap='Greens', fmt='g', linecolor='Black', linewidths=.005)
    plt.ylabel('Prawdziwa etykieta')
    plt.xlabel('Sklasyfikowana etykieta')
    plt.show()


def load_data(transform, batch_size):
    train_data = datasets.MNIST(r'.\data', download=True, train=True, transform=transform)
    test_data = datasets.MNIST(r'.\data', download=True, train=False, transform=transform)
    images_data = {
        'train': train_data,
        'val': test_data,
    }
    data_loaders = {x: torch.utils.data.DataLoader(
        images_data[x], batch_size=batch_size, shuffle=True) for x in ['train', 'val']}
    return data_loaders


def prepare_results(predicted, labels):
    labels_t = labels[0].cpu().data.numpy()
    output_t = predicted[0].cpu().data.numpy()
    for i in range(1, len(predicted)):
        labels_tmp = labels[i].cpu().data.numpy()
        output_tmp = predicted[i].cpu().data.numpy()
        labels_t = np.append(labels_t, labels_tmp)
        output_t = np.append(output_t, output_tmp)
    cm = confusion_matrix(y_true=labels_t, y_pred=output_t)
    return cm


def print_incorrectly_predicted_images(images, predicted, labels, own_architecture):
    for i in range(5):
        plt.subplot(1, 5, i + 1)
        plt.tight_layout()
        if own_architecture:
            plt.imshow(images[i].cpu().data.numpy().squeeze(), cmap='gray_r')
        else:
            plt.imshow(images[i].cpu().data.numpy().transpose(1, 2, 0).astype('uint8'), cmap='gray_r')
        plt.title("P=" + str(predicted[i]) + ", T=" + str(labels[i]))
        plt.gca().axes.get_xaxis().set_visible(False)
        plt.gca().axes.get_yaxis().set_visible(False)
    plt.show()


def train_model(model, loader_train_data, criterion, optimizer, num_epochs, model_path):
    device = torch.device("cuda")
    model.to(device)
    predicted_from_last_epoch = []
    labels_from_last_epoch = []
    start_time = time()
    for epoch in range(1, num_epochs + 1):
        model.train()
        running_loss = 0.0
        predicted_correctly = 0
        for inputs, labels in loader_train_data:
            inputs = inputs.to(device)
            labels = labels.to(device)
            optimizer.zero_grad()
            with torch.set_grad_enabled(True):
                outputs = model(inputs)
                loss = criterion(outputs, labels)
                _, predictions = torch.max(outputs, 1)
                loss.backward()
                optimizer.step()
            running_loss += loss.item() * inputs.size(0)
            predicted_correctly += torch.sum(predictions == labels.data)
            if epoch == num_epochs:
                predicted_from_last_epoch.append(predictions)
                labels_from_last_epoch.append(labels)
        epoch_loss = running_loss / len(loader_train_data.dataset)
        epoch_accuracy = predicted_correctly.double() / len(loader_train_data.dataset)
        print('{:.4f}, {:.4f}'.format(epoch_loss, epoch_accuracy))
    time_elapsed = time() - start_time
    print('Training time: {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    torch.save(model, model_path)
    cm = prepare_results(predicted_from_last_epoch, labels_from_last_epoch)
    plot_confusion_matrix(cm, 'train')
    return model


def test_model(model, loader_test_data, batch_size, own_architecture):
    model.eval()
    with torch.no_grad():
        labels_list = []
        predicted_list = []
        incorrectly_predicted_images = []
        incorrectly_predicted_classes = []
        true_labels = []
        for images, labels in loader_test_data:
            output = model(images.cuda())
            _, predicted = torch.max(output.data, 1)
            for x in range(batch_size):
                if predicted[x] != labels[x]:
                    incorrectly_predicted_images.append(images[x])
                    predicted_tmp = predicted[x].data.item()
                    incorrectly_predicted_classes.append(predicted_tmp)
                    label_tmp = labels[x].data.item()
                    true_labels.append(label_tmp)
            predicted_list.append(predicted)
            labels_list.append(labels)
        print('Accuracy: {:.2f}%'.format((10000 - len(incorrectly_predicted_classes)) / 100))
        print_incorrectly_predicted_images(incorrectly_predicted_images, incorrectly_predicted_classes, true_labels,
                                           own_architecture)
        cm = prepare_results(predicted_list, labels_list)
        plot_confusion_matrix(cm, 'val')
