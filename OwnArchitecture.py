# -*- coding: utf-8 -*-

import torch
from torch import nn
from torch import optim
from torchsummary import summary
from torchvision import transforms

import Service


class Network(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv2d(32, 64, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2))
        self.drop_out = nn.Dropout()
        self.fc1 = nn.Linear(7 * 7 * 64, 10)

    def forward(self, x):
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.reshape(out.size(0), -1)
        out = self.drop_out(out)
        out = self.fc1(out)
        return out


epochs = 200
learning_rate = 0.001
momentum = 0.9
batch_size = 50
loadModel = False
model_path = './oa_' + str(epochs) + '_' + str(learning_rate) + '_' + str(batch_size) + '_' + str(momentum) + '.pt'
transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.1307,), (0.3081,))])
data_loaders = Service.load_data(transform, batch_size)

input_model = Network()
criterion = nn.CrossEntropyLoss()
# optimizer = optim.Adam(input_model.parameters(), lr=learning_rate)
optimizer = optim.SGD(input_model.parameters(), lr=learning_rate, momentum=momentum)

if not loadModel:
    model = Service.train_model(input_model, data_loaders['train'], criterion, optimizer, epochs, model_path)
    Service.test_model(model, data_loaders['val'], batch_size, True)
else:
    try:
        model = torch.load(model_path)
    except FileNotFoundError:
        model = Service.train_model(input_model, data_loaders['train'], criterion, optimizer, epochs, model_path)
    Service.test_model(model, data_loaders['val'], batch_size, True)
    # summary(model, (1, 28, 28))
